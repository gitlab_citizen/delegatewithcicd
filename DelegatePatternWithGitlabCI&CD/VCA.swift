//
//  VCA.swift
//  DelegatePatternWithGitlabCI&CD
//
//  Created by Gianni Gianino on 06/09/21.
//


import UIKit

class VCA: UIViewController, VCBDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if let nav = segue.destination as? UINavigationController, let VCB = nav.topViewController as? VCB {
            VCB.delegate = self
        }
    }
    
    func changeBackgroundColor(_ color: UIColor?) {
        view.backgroundColor = color
    }
    
    
    
    
}

