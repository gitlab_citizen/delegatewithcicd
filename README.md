# DelegateWithCICD

<div align="center">
	<img width="323" height="183" src="Assets/Logo/logo.png">
</div>

## Indice


- [Badges](#badges)
- [Progetto](#progetto)
  - [Descrizione](#descrizione)
  - [Dipendenze](#dipendenze)
- [Gitlab CI](#gitlab-ci-pipeline)
    - [Concetti Principali](#concetti-principali)
    - [Pipeline](#pipeline)
    - [Gitlab Runner](#gitlab-runner)
        - [Installazione](#installazione)
        - [Registrazione](#registrazione)
- [Utilizzo della Pipeline](#utilizzo-della-pipeline)
- [Approfondimenti](#approfondimenti)


#### Badges


[![pipeline status](https://gitlab.com/gitlab_citizen/delegatewithcicd/badges/main/pipeline.svg)](https://gitlab.com/gitlab_citizen/delegatewithcicd/-/commits/main) [![coverage report](https://gitlab.com/gitlab_citizen/delegatewithcicd/badges/main/coverage.svg)](https://gitlab.com/gitlab_citizen/delegatewithcicd/-/commits/main) 


### Progetto


#### Descrizione


- Delegate Pattern con un semplice esempio di Gitlab CI Pipeline.
- La seguente guida riguarda la parte di CI che si può aggiornare quando verrà aggiunta anche la parte di CD.

#### Dipendenze

- E' necessario installare **XCPretty** per eseguire gli scripts del comando **xcodebuild**. Per installarlo eseguire il comando da terminale `gem install xcpretty`



### Gitlab CI


#### Concetti principali

- **CI (Continuos Integration)**: Si tratta di creare una serie di scripts da avviare ad ogni push di codice nel repo. Alcuni esempi di tali script possono essere il build dell'App o l'avvio della test suite.

- **CD (Continuos Deployment)**: Il deployment dell'App viene fatto in automatico senza l'intervento manuale dello sviluppatore

#### Pipeline

- **Gitlab CI Pipeline**: una pipeline non è altro che una lista di jobs da eseguire, definiti nel file `.gitlab-ci.yml`.</br></br>


    * Questi jobs sono eseguiti da un altro componente della pipeline di Gitalb detto **Runner**. </br></br>

    * Quando la pipeline viene eseguita il Runner prende un job dal file `.gitlab-ci.yml` e lo esegue usando **l'Executors** scelto. Alcuni degli executors disponibili sono per esempio **Docker**, **Kubernetes** o la semplice **Shell** di sistema. </br></br>
    
    * Nel progetto corrente è stato scelto come executor la **Shell**, eventualmente in un secondo momento si può passare a usare **Docker** in cui i jobs verranno eseguiti in un determinato container e tutte le dipendenze necessarie possono essere messe in una **Docker Image** definita all'inizio del file `.gitlab-ci.yml`.


#### Gitlab Runner

##### Installazione

- Le istruzioni per l'installazione su piattaforma macOS sono disponibili qui [Gitlab Runner Install](https://docs.gitlab.com/runner/install/osx.html) </br></br>

- Per quanto riguarda l'installazione si consiglia di eseguire quella manuale e non quella tramite il gestore di pacchetti **Brew** come già menzionato nel documento


##### Registrazione

- A questo punto si tratta di registrare il runner appena installato con il progetto/repository su Gitlab che vogliamo. Per fare ciò si rimanda questa semplice guida che ne illustra i vari passaggi

- [Runner Registration Tutorial](https://faun.pub/continuous-integration-in-ios-apps-using-gitlab-ci-11156f232087)

    * I primi passaggi si possono saltare fino alla sezione che mostra la registrazione del runner. </br></br>

### Utilizzo Della Pipeline

- Nel progetto sono stati aggiunti 2 Unit Test. </br></br>
    * Eseguire il Runner precedentemente installato e registrato con il progetto su Gitlab con il comando `gitlab-runner run`. </br></br>
    * Commentare la riga di codice `vca = nil` nel file `DelegatePatternWithGitlabCICDTests.swift`. </br></br>
    * Committare la modifica fatta e selezionare nel progetto Gitlab la voce CI/CD > Pipeline per vedere la pipeline. La pipeline viene aggiunta appena il runner prende in carico la lista dei jobs da eseguire, per capire quando questo avviene controllare il terminale da cui è stato eseguito tale Runner. </br></br>

    * A questo punto ci si accorgera che la pipeline una volta avviata non verrà completata in quanto fallirà per via di 1 Unit Test non passato. Rimuovendo di nuovo il commento alla riga iniziale ed eseguendo nuovamente il commit questa volta la pipeline verrà terminata con successo. </br></br>

    * I dettagli della pipeline si possono vedere premendo sullo stato della pipeline (running, passed etc..) o sul suo ID.



### Approfondimenti

- Per eventuali approfondimenti di Gitlab CI si rimanda alla documentazione ufficiale [Gitlab CI Docs](https://docs.gitlab.com/ee/ci/)