//
//  DelegatePatternWithGitlabCICDTests.swift
//  DelegatePatternWithGitlabCI&CDTests
//
//  Created by Gianni Gianino on 06/09/21.
//

import XCTest
@testable import DelegatePatternWithGitlabCICD

class DelegatePatternWithGitlabCI_CDTests: XCTestCase {
    
    
    var vcb: VCB!
    var vca: VCA!
    
    override func setUpWithError() throws {
        super.setUp()
        vcb = VCB()
        vca = VCA()
        vcb.delegate = vca
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        vcb = nil
        super.tearDown()
    }
    
    func testVCBHasCorrectDelegate() throws {
        
        let expectation = XCTestExpectation(description: "Check VCB has delegate as VCA instance")
        
        XCTAssertNotNil(vcb.delegate!, "VCB delegate property is nil")
        
        XCTAssert((vcb.delegate)! is VCA, "VCB delegate instance is not of type VCA")
        
        expectation.fulfill()
    }
    
    func testVCBDelegateHasWeakReference() throws {
        
        let expectation = XCTestExpectation(description: "Check VCB delegate has weak reference")

        vca = nil
        XCTAssertTrue(vcb.delegate == nil, "VCB delegate property is not nil after removing VCA class reference")
        
        expectation.fulfill()
    }
    
    func testPerformanceExample() throws {
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
